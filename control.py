#!/usr/bin/env python3

import subprocess

main_menu_spec = [
   ( 'Read and process NASTRAN panel file', 1 ), 
   ( 'Process CFD/NASTRAN interpolation', 2 ),
   ( 'Run CFD solver', 3 ),
   ( 'Process NASTRAN/CFD interpolation', 4 ),
   ( 'Exit', 8 )
]

npf_menu_spec = [
   ( 'Goto Main menu', 0 ),
   ( 'Run PROGRAM to process NASTRAN panel file', 1 ),
   ( 'Set input file name', 2 ),
   ( 'Set output file name', 3 ),
   ( 'Exit', 8 )
]

panel_input_filename = 'panel-definice.dat'
panel_output_filename = 'panel-coord.dat'

def menu(spec):
   maxl = len( max( spec, key = lambda item : len( item[0] ) )[0] )

   while True:
      print( 'Select one of these alternatives:' )
      print( '-' * (maxl + 6) )

      for i in spec:
         s = i[0]
         l = len( s )
         s += ' '
         s = s.ljust( maxl + 4, '_' ) 
         print( s, i[1] )

      print( '-' * (maxl + 6) )
      ans = input( 'Your answer: ')

      try:
         choice = int( ans )

         for i in spec:
            if choice == i[1]:
               return choice
      except:
         pass

def process_panel_file():
   p = subprocess.Popen('./IA3_read_panel.x', shell = True, stdin = subprocess.PIPE)

   params = [ panel_input_filename + '\n', panel_output_filename + '\n' ]
   for i in params:
      p.stdin.write( i.encode('utf-8') )

   p.stdin.close()
   p.wait()

def get_panel_input_filename():
   global panel_input_filename

   s = 'SET INPUT FILE NAME (current '
   s += panel_input_filename
   s += ' = ENTER): '
   ans = input( s )
   if ans != '':
      panel_input_filename = ans

def get_panel_output_filename():
   global panel_output_filename

   s = 'SET OUTPUT FILE NAME (current '
   s += panel_output_filename
   s += ' = ENTER): '
   ans = input( s )
   if ans != '':
      panel_output_filename = ans

def npf_menu():
   while True:
      choice = menu(npf_menu_spec)

      if choice == 0:
         return
      elif choice == 1:
         process_panel_file()
      elif choice == 2:
         get_panel_input_filename()
      elif choice == 3:
         get_panel_output_filename()
      elif choice == 8:
         exit()

def main_menu():
   while True:
      choice = menu(main_menu_spec)

      if choice == 8:
         exit()
      elif choice == 1:
         npf_menu()

def main():
   main_menu()

main()
